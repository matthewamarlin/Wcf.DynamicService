param([int] $VersionMajor = 1, [int] $VersionMinor = 0, [int] $VersionRevision = 0, [int] $VersionBuildNumber = 0, [string] $VersionMetadata = "local")


$SolutionDirectory = "..\Source"

<# 
    Set PSScriptRoot
#>
if([String]::IsNullOrEmpty($PSScriptRoot))
{
    $PSScriptRoot = Split-Path -Parent $MyInvocation.MyCommand.Definition;
}

Write-Host "PSScriptRoot: $PSScriptRoot"

$ErrorActionPreference = "Stop"

$VersionFull = "$VersionMajor.$VersionMinor.$VersionRevision"

if(-not [string]::IsNullOrWhiteSpace($VersionMetadata))
{
    $VersionFull = "$VersionFull-$VersionMetadata"
}

<#
    Clean up any old generated files and create fresh folders.
#>

if(Test-Path "$PSScriptRoot\obj")
{
    Write-Host "Cleaning obj folder ($PSScriptRoot\obj)."
    Remove-Item "$PSScriptRoot\obj" -Force -Recurse
}

if(Test-Path "$PSScriptRoot\bin")
{
    Write-Host "Cleaning bin folder ($PSScriptRoot\bin)."
    Remove-Item "$PSScriptRoot\bin" -Force -Recurse
}

New-Item obj -ItemType Directory
New-Item bin -ItemType Directory

<#
    Build with MSBuild And Run Octopack
    http://docs.octopusdeploy.com/display/OD/Using+OctoPack
#>

Set-Alias build -Value "C:\Program Files (x86)\MSBuild\12.0\Bin\amd64\MSBuild.exe"

build $SolutionDirectory\SpearOne.DynamicService.sln /t:Build /p:RunOctoPack=true /p:OctoPackPackageVersion="$VersionFull" /p:OctoPackNuGetProperties="ProductVersion=$VersionFull"

<#
    Copy OctoPacked Packages
#>

Get-ChildItem $SolutionDirectory\*\obj\octopacked\*.nupkg | Copy-Item -Destination .\bin

nuget pack $SolutionDirectory\DataTransferObjects\DataTransferObjects.csproj -Version $VersionFull -Properties ProductVersion="$VersionFull" -OutputDirectory .\bin 
nuget pack $SolutionDirectory\Contracts\Contracts.csproj -Version $VersionFull -Properties ProductVersion="$VersionFull" -OutputDirectory .\bin
nuget pack $SolutionDirectory\DynamicProxy\DynamicProxy.csproj -Version $VersionFull -Properties ProductVersion="$VersionFull" -OutputDirectory .\bin
nuget pack $SolutionDirectory\Proxy\Proxy.csproj -Version $VersionFull -Properties ProductVersion="$VersionFull" -OutputDirectory .\bin