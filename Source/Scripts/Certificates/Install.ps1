Set-Location $PSScriptRoot

Import-Module pki

$pwd = ConvertTo-SecureString -String "%7#Ge*s3@@@" -Force –AsPlainText

Import-Certificate -FilePath ".\SpearOne Root CA.crt" -CertStoreLocation "Cert:\LocalMachine\AuthRoot"
Import-PfxCertificate -FilePath ".\localhost.pfx" -Password $pwd -Exportable -CertStoreLocation "cert:\LocalMachine\My"
Import-PfxCertificate -FilePath ".\Generic Client Certificate.pfx" -Password $pwd -Exportable -CertStoreLocation "cert:\CurrentUser\My"