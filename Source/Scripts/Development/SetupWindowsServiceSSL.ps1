﻿<#
	This script is used to setup SSL (when using Transport Security) & URL Reservations for IIS Express.
	The certificate should be placed in the Local Machine Personal Store
#>
Write-Host "Configuring Windows Service SSL"

<#
	Enter your Server Certificate (Found in Certs > Local Machine > Personal ) Below:
	Example: "6c41c45b7219d371a2074553753ec2f1b3eaf5e7" 
#>
$CertificateThumbprint="29b0db0b7b4b372a86de7d27cdbd58b212204d55" 

<# 
	Enter the port for your IIS Instance (As defined in the Properties > Web)  
#>
$Port = 9210

<# 
	The appid doesnt matter here, you can use anything, its just a referenzce. 
	(But it must be unique to the reservation, so generate a new Guid per project)
#>
netsh "http" "add" "sslcert" "ipport=0.0.0.0:$Port" "appid={879B83EB-AE0A-4901-983F-F46AE12BB69A}" "certhash=$CertificateThumbprint"
netsh "http" "add" "urlacl" "url=https://localhost:$Port/" "user=Everyone"

Write-Host "Script Complete"