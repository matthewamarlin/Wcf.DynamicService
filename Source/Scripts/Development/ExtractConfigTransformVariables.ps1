$regex = '#{.*}'

foreach($transformFile in (Get-ChildItem ..\..\Service\Configuration\Transforms\*.xdt)){
    Get-Content $transformFile | Select-String -Pattern $regex | % { $_.Matches } | % { $_.Value } 
    Write-Host
} 