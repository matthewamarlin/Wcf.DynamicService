<#
	This script is used to Remove SSL Configuration & URL Reservations for IIS Express
#>

$Port = 9210;

Write-Host "Deleting SSL Configuration for Windows Service on port $Port"

netsh "http" "delete" "sslcert" "ipport=0.0.0.0:$Port"
netsh "http" "delete" "urlacl" "url=https://localhost:$Port/"

Write-Host "Script Complete"