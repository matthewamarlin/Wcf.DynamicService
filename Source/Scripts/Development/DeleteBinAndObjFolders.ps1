Set-Location $PSScriptRoot

Get-ChildItem -Path ..\ -Recurse | foreach {

    if(($_.Name -eq "bin") -or ($_.Name -eq "obj"))
    {
        Write-Host "Removing $($_.FullName)'"
        Remove-Item -Path "$($_.FullName)\" -Recurse -Force
    }
}
