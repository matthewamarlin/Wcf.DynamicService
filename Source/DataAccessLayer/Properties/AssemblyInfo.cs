﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.DynamicService.DataAccessLayer")]
[assembly: AssemblyDescription("The data access layer for the SpearOne.DynamicService")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("D7229979-410F-43C9-AD15-7EDB409A888F")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]