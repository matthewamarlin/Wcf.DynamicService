﻿using SpearOne.DynamicService.DataAccessLayer.Entities;

namespace SpearOne.DynamicService.DataAccessLayer.Interfaces
{
    public interface IDataController
    {
        void SetValue(Entity entity);
        Entity GetValue();
    }
}
