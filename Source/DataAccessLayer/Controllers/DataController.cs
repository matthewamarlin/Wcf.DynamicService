﻿using System.IO;
using System.Runtime.Serialization;
using SpearOne.DynamicService.DataAccessLayer.Entities;
using SpearOne.DynamicService.DataAccessLayer.Interfaces;

namespace SpearOne.DynamicService.DataAccessLayer.Controllers
{
    public class DataController : IDataController
    {
        private readonly string _filePath;

        public DataController(string filePath)
        {
            _filePath = filePath;
        }

        public void SetValue(Entity entity)
        {
            File.WriteAllText(_filePath, Serialize(entity)); 
        }

        public Entity GetValue()
        {
            if (!File.Exists(_filePath)) return null;

            return Deserialize<Entity>(File.ReadAllText(_filePath));
        }

        public static string Serialize(object obj)
        {
            using (var memoryStream = new MemoryStream())
            using (var reader = new StreamReader(memoryStream))
            {
                var serializer = new DataContractSerializer(obj.GetType());
                serializer.WriteObject(memoryStream, obj);
                memoryStream.Position = 0;
                return reader.ReadToEnd();
            }
        }

        public static T Deserialize<T>(string xml)
        {
            using (Stream stream = new MemoryStream())
            {
                var data = System.Text.Encoding.UTF8.GetBytes(xml);
                stream.Write(data, 0, data.Length);
                stream.Position = 0;
                var deserializer = new DataContractSerializer(typeof(T));
                return (T) deserializer.ReadObject(stream);
            }
        }
    }
}
