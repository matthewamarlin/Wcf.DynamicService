﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceProcess;
using System.Text;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using SpearOne.DynamicService.Service;

namespace SpearOne.DynamicService.WindowsService
{
    partial class ServiceHost : ServiceBase
    {
        private static System.ServiceModel.ServiceHost _serviceHost;
        private static ServiceType _serviceType = ServiceType.Dynamic;
        private static readonly Log4NetWrapper Log = new Log4NetWrapper(typeof(ServiceHost));

        public ServiceHost()
        {
            InitializeComponent();
        }

        #region Overrides of ServiceBase

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
            StartService(args);
        }

        protected override void OnStop()
        {           
            StopService();
            base.OnStop();
        }

        #endregion

        public static void StartService(string[] args)
        {
            Log.Write(Level.Info, args.Length);

            if (args.Length > 1)
            {
                Log.Write(Level.Error, "Too many arguments, please specify either 'Dynamic' or 'Standard' as the start parameter.");
            }
            else if (args.Length == 1)
            {
                Log.WriteFormat(Level.Info, "Starting service with parameter: {0}", args[0]);

                ServiceType serviceType;
                if (!Enum.TryParse(args[0], out serviceType))
                {
                    Log.WriteFormat(Level.Error,
                        "Could not parse start parameter '{0}', please specify either 'Dynamic' or 'Standard' as the start parameter.",
                        args[0]);
                }
                else
                {
                    StartService(serviceType);
                }
            }
            else
            {
                Log.Write(Level.Info, "Starting service with defualt settings.");

                StartService(ServiceType.Dynamic);
            }
        }

        public static void StartService(ServiceType serviceType)
        {
            _serviceType = serviceType;

            try
            {
                _serviceHost = serviceType == ServiceType.Dynamic ?
                    new System.ServiceModel.ServiceHost(typeof(Service.DynamicService)) :
                    new System.ServiceModel.ServiceHost(typeof(StandardService));

                _serviceHost.Open();
                _serviceHost.Faulted += ServiceHost_Faulted;

                LogServiceHostInfo();
            }
            catch (Exception exception)
            {
                Log.WriteFormat(Level.Error, "An exception occured when attepting to start the service, {0}", exception.Message);
            }
        }

        private static void StopService()
        {
            if (_serviceHost == null || _serviceHost.State != CommunicationState.Closed) return;

            if (_serviceHost.State != CommunicationState.Faulted)
            {
                _serviceHost.Close();
                Log.WriteFormat(Level.Info, "{0} ServiceHost Closed.", _serviceHost.Description.Name);
            }
            else
            {
                _serviceHost.Abort();
                Log.WriteFormat(Level.Info, "{0} ServiceHost Aborted.", _serviceHost.Description.Name);
            }
        }

        public static void RestartService()
        {
            StopService();
            StartService(_serviceType);
        }

        private static void LogServiceHostInfo()
        {
            if (_serviceHost == null)
            {
                const string message = "An internal error occured, the ServiceHost was not set to an instance of an object.";
                Log.Write(Level.Error, message);
                throw new InvalidOperationException(message);
            }

            var stringBuilder = new StringBuilder();
            stringBuilder.AppendFormat("{0} Connection Details:", _serviceHost.Description.Name);
            stringBuilder.AppendLine();

            // Behaviors
            var annotation = _serviceHost.Description.Behaviors.Find<ServiceBehaviorAttribute>();
            stringBuilder.AppendLine("Service Behaviors: ");
            stringBuilder.AppendFormat("\tConcurrency Mode = {0}", annotation.ConcurrencyMode).AppendLine();
            stringBuilder.AppendFormat("\tInstanceContext Mode = {0}", annotation.InstanceContextMode).AppendLine();
            stringBuilder.AppendLine();

            // Endpoints
            stringBuilder.Append("Endpoints Exposed:").AppendLine();
            foreach (var endPoint in _serviceHost.Description.Endpoints)
            {
                stringBuilder.AppendFormat("\t{0} at {1} with {2} binding; "
                    , endPoint.Contract.ContractType.Name
                    , endPoint.Address
                    , endPoint.Binding.Name).AppendLine();
            }
            stringBuilder.AppendLine();

            // Metadata
            var metabehaviour = _serviceHost.Description.Behaviors.Find<ServiceMetadataBehavior>();
            if (metabehaviour != null)
            {
                stringBuilder.AppendLine("Service Metadata:");
                if (metabehaviour.HttpGetEnabled)
                {
                    stringBuilder.AppendFormat("\tHttpGet Address: {0}\n",
                        metabehaviour.HttpGetUrl == null ? _serviceHost.BaseAddresses[0] : metabehaviour.HttpGetUrl);
                }
                else
                {
                    stringBuilder.AppendLine("\tHttpGet: Disabled");
                }
                if (metabehaviour.HttpsGetEnabled)
                {
                    stringBuilder.AppendFormat("\tHttpsGet Address: {0}\n", 
                        metabehaviour.HttpsGetUrl == null ? _serviceHost.BaseAddresses[0] : metabehaviour.HttpsGetUrl);
                }
                else
                {
                    stringBuilder.AppendLine("\tHttpGets: Disabled");
                }
                if (metabehaviour.ExternalMetadataLocation != null)
                {
                    stringBuilder.AppendFormat("\tExternal Address: {0}", metabehaviour.ExternalMetadataLocation);
                }
            }
            stringBuilder.AppendLine();

            Log.Write(Level.Info, stringBuilder.ToString());
        }

        private static void ServiceHost_Faulted(Object sender, EventArgs e)
        {
            Log.Warn(string.Format("{0} Faulted. Attempting Restart.", _serviceHost.Description.Name));
            RestartService();
        }
    }
}
