﻿using System;
using System.ServiceProcess;
using System.Threading;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using SpearOne.DynamicService.Service;

namespace SpearOne.DynamicService.WindowsService
{
    public class ServiceInitializer
    {
        private static readonly Log4NetWrapper Log = new Log4NetWrapper(typeof(ServiceInitializer));

        public ServiceInitializer()
        {
            ServiceCompositionRoot.Initialize();
        }

        public void StartConsoleHost(ServiceType serviceType)
        {
            try
            {
                ServiceHost.StartService(serviceType);
            }
            catch (Exception ex)
            {
                Log.WriteFormat(Level.Error, "An error occured: \n{0}", ex.Message);
            }

            Thread.Sleep(Timeout.Infinite);
        }

        public void StartServiceHost()
        {
            try
            {
                var servicesToRun = new ServiceBase[] { new ServiceHost(), };
                ServiceBase.Run(servicesToRun);
            }
            catch (Exception ex)
            {
                Log.WriteFormat(Level.Error, "An error occured: \n{0}", ex);
            }
        }
    }
}
