﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.DynamicService.Hosts.WindowsService")]
[assembly: AssemblyDescription("An implementation of the SpearOne.DynamicService with a WindowsService Shell, The service remains unconfigured in this package.")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("58384826-00F2-44FF-A6A4-17FCEFFD8BF4")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]