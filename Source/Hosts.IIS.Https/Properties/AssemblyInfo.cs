﻿using System.Reflection;
using System.Runtime.InteropServices;
using SpearOne.DynamicService.Service;

[assembly: AssemblyTitle("SpearOne.DynamicService.Hosts.IIS.Https")]
[assembly: AssemblyDescription("An implementation of the 'SpearOne.DynamicService' hosted as an IIS service using the Https Protocol")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("8823A567-82D8-406A-924C-3CA167B705BF")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]
[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(ServiceCompositionRoot), "Initialize")]