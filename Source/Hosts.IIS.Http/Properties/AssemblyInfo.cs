﻿using System.Reflection;
using System.Runtime.InteropServices;
using SpearOne.DynamicService.Service;

[assembly: AssemblyTitle("SpearOne.DynamicService.Hosts.IIS.Http")]
[assembly: AssemblyDescription("An implementation of the 'SpearOne.DynamicService' hosted as an IIS service, using the Http protocol.")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("349AEB06-D1BF-4BD1-AA91-B9F58C6613E1")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]
[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(ServiceCompositionRoot), "Initialize")]