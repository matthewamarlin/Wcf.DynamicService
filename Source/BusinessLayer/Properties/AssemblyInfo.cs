﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.DynamicService.BusinessLayer")]
[assembly: AssemblyDescription("The assembly containing the business logic for the SpearOne.DynamicService")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("D0CD12C7-DF0C-4BCB-A531-D0F572FAB4A2")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]