﻿using AutoMapper;
using SpearOne.DynamicService.BusinessLayer.Interfaces;
using SpearOne.DynamicService.DataAccessLayer.Entities;
using SpearOne.DynamicService.DataAccessLayer.Interfaces;
using SpearOne.DynamicService.DataTransferObjects;

namespace SpearOne.DynamicService.BusinessLayer
{
    public class ValueController : IValueController
    {
        private readonly IDataController _dataController;

        public ValueController(IDataController dataController)
        {
            _dataController = dataController;

            Mapper.CreateMap<Value, Entity>().ConstructUsing(value => new Entity() { Column = value.Message });
            Mapper.CreateMap<Entity, Value>().ConstructUsing(entity => new Value() { Message = entity.Column});
        }

        public void SetValue(Value value)
        {
            var entity = Mapper.Map<Entity>(value);

            _dataController.SetValue(entity);
        }

        public Value GetValue()
        {

            var entity = _dataController.GetValue();
            var value = Mapper.Map<Value>(entity);

            return value;
        }
    }
}
