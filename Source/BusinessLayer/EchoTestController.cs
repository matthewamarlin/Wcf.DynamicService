﻿using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using SpearOne.DynamicService.BusinessLayer.Interfaces;

namespace SpearOne.DynamicService.BusinessLayer
{
    public class EchoTestController : IEchoTestController
    {
        private readonly ILogger _log = new Log4NetWrapper(typeof(EchoTestController));

        public string Echo(string message)
        {
            _log.WriteFormat(Level.Debug, "Recieved Echo Test Message [{0}]", message);
            return message;
        }
    }
}