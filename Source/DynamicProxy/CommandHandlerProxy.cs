﻿using System.Linq;
using System.ServiceModel;
using SpearOne.DynamicService.Contracts;

namespace SpearOne.DynamicService.DynamicProxy
{
    public class CommandHandlerProxy<TCommand> : ICommandHandler<TCommand>
    {
        public void Handle(TCommand command)
        {
            var channelFactory = new ChannelFactory<IDynamicService>("DynamicServiceEndpoint");

            var service = channelFactory.CreateChannel();

            try
            {
                object result = service.ExecuteCommand(command);

                Update(result, command);
            }
            finally
            {
                var connection = service as ICommunicationObject;

                if (connection != null)
                {
                    try
                    {
                        connection.Close();
                    }
                    catch
                    {
                        connection.Abort();
                    }
                }
            }
        }

        private static void Update(object source, object destination)
        {
            var properties =
                from property in destination.GetType().GetProperties()
                where property.CanRead && property.CanWrite
                select property;

            foreach (var property in properties)
            {
                object value = property.GetValue(source, null);

                property.SetValue(destination, value, null);
            }
        }
    }
}
