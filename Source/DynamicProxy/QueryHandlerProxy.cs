﻿using System.ServiceModel;
using SpearOne.DynamicService.Contracts;

namespace SpearOne.DynamicService.DynamicProxy
{
    public class QueryHandlerProxy<TQuery, TResult> : IQueryHandler<TQuery, TResult>
        where TQuery : IQuery<TResult>
    {
        public TResult Handle(TQuery query)
        {
            IDynamicService service = null;

            try
            {
                var channelFactory = new ChannelFactory<IDynamicService>("DynamicServiceEndpoint");

                service = channelFactory.CreateChannel();

                return (TResult) service.ExecuteQuery(query);
            }
            finally
            {
                var connection = service as ICommunicationObject;

                if (connection != null)
                {
                    try
                    {
                        connection.Close();
                    }
                    catch
                    {
                        connection.Abort();
                    }
                }
            }
        }
    }
}
