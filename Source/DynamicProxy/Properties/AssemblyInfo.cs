﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.DynamicService.DynamicProxy")]
[assembly: AssemblyDescription("A dynamic client proxy for communicating with the SpearOne.DynamicService. (.Net clients only)")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("9070C3A6-1152-452D-B26B-4A2A0935F43C")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]