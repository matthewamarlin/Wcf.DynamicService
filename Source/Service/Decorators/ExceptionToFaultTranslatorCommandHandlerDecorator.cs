﻿using System;
using System.Linq;
using System.ServiceModel;
using SpearOne.Common.Extensions;
using SpearOne.DynamicService.Contracts;

namespace SpearOne.DynamicService.Service.Decorators
{
    public class ExceptionToFaultTranslatorCommandHandlerDecorator<TCommand>: ICommandHandler<TCommand>
    {
        private readonly ICommandHandler<TCommand> _innerCommandHandler;

        public ExceptionToFaultTranslatorCommandHandlerDecorator(ICommandHandler<TCommand> innerCommandHandler)
        {
            if(innerCommandHandler == null)
                throw new ArgumentNullException(nameof(innerCommandHandler));

            _innerCommandHandler = innerCommandHandler;
        }

        public void Handle(TCommand command)
        {
            try
            {
                _innerCommandHandler.Handle(command);
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception exception)
            {
                var message = String.Join(Environment.NewLine + " -> ", exception.FromHierarchy(ex => ex.InnerException).Select(ex => ex.Message));
                throw new FaultException(message);
            }
        }
    }
}
