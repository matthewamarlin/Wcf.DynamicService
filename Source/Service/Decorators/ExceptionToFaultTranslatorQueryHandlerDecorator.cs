﻿using System;
using System.Linq;
using System.ServiceModel;
using SpearOne.Common.Extensions;
using SpearOne.DynamicService.Contracts;
using static System.String;

namespace SpearOne.DynamicService.Service.Decorators
{
    public class ExceptionToFaultTranslatorQueryHandlerDecorator<TQuery, TResult> : IQueryHandler<TQuery, TResult> where TQuery : IQuery<TResult>
    {
        private readonly IQueryHandler<TQuery, TResult> _innerQueryHandler;

        public ExceptionToFaultTranslatorQueryHandlerDecorator(IQueryHandler<TQuery, TResult> innerQueryHandler)
        {
            if (innerQueryHandler == null)
                throw new ArgumentNullException(nameof(innerQueryHandler));

            _innerQueryHandler = innerQueryHandler;
        }

        public TResult Handle(TQuery query)
        {
            try
            {
                return _innerQueryHandler.Handle(query);
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception exception)
            {
                var message = Join(Environment.NewLine + " -> ", exception.FromHierarchy(ex => ex.InnerException).Select(ex => ex.Message));
                throw new FaultException(message);
            }
        }
    }
}
