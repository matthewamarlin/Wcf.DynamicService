using System;
using System.Collections.Generic;
using System.Reflection;
using System.ServiceModel;
using log4net.Config;
using SimpleInjector;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using SpearOne.DynamicService.BusinessLayer;
using SpearOne.DynamicService.BusinessLayer.Interfaces;
using SpearOne.DynamicService.Contracts;
using SpearOne.DynamicService.DataAccessLayer.Controllers;
using SpearOne.DynamicService.DataAccessLayer.Interfaces;
using SpearOne.DynamicService.Handlers.Decorators;
using SpearOne.DynamicService.Service.Decorators;

namespace SpearOne.DynamicService.Service
{
    public static class ServiceCompositionRoot
    {
        private static readonly ILogger Log = new Log4NetWrapper(typeof(ServiceCompositionRoot));

        private const string ContainerNotSetMessage = "The dependency injection container has not been initialised. Please " +
                                                      "call the Initilize method on the ServiceCompositionRoot before requesting any types.";
        private static Container _container;

        public static void Initialize()
        {
            try
            {
                XmlConfigurator.Configure();

                Log.Write(Level.Info, "SpearOne.DynamicService Initialising");
                
                _container = new Container();

                /* Handler Composition */
                var assembly = Assembly.Load("SpearOne.DynamicService.Handlers");

                _container.Register(typeof(ICommandHandler<>), new List<Assembly> { assembly });

                _container.RegisterDecorator(typeof(ICommandHandler<>), typeof(LogTracingCommandHandlerDecorator<>));
                _container.RegisterDecorator(typeof(ICommandHandler<>), typeof(ExceptionLoggingCommandHandlerDecorator<>));

                _container.Register(typeof(IQueryHandler<,>), new List<Assembly> { assembly });
                _container.RegisterDecorator(typeof(IQueryHandler<,>), typeof(LogTracingQueryHandlerDecorator<,>));
                _container.RegisterDecorator(typeof(IQueryHandler<,>), typeof(ExceptionLoggingQueryHandlerDecorator<,>));

            #if DEBUG
                _container.RegisterDecorator(typeof(ICommandHandler<>), typeof(PerformanceTimerCommandHandlerDecorator<>));
                _container.RegisterDecorator(typeof(IQueryHandler<,>), typeof(PerformanceTimerQueryHandlerDecorator<,>));
            #endif

                /* Business & Data Access Layer Composition */
                _container.Register(typeof(IEchoTestController), typeof(EchoTestController));
                _container.Register(typeof(IValueController), typeof(ValueController));
                _container.Register(typeof(IDataController), () => new DataController(@".\SavedEntity.txt") );


                /* Wcf Specific Registrations */
                _container.RegisterDecorator(typeof(ICommandHandler<>), typeof(ExceptionToFaultTranslatorCommandHandlerDecorator<>));
                _container.RegisterDecorator(typeof(IQueryHandler<,>), typeof(ExceptionToFaultTranslatorQueryHandlerDecorator<,>));

                /* Only use when using the ServiceHost Factory in the .svc files. */
                //_container.EnablePerWcfOperationLifestyle();
                //_container.RegisterWcfServices(Assembly.GetExecutingAssembly());
                //_container.Register(typeof(IDynamicService), typeof(DynamicService));
                //SimpleInjectorServiceHostFactory.SetContainer(_container);

                _container.Verify();
            }
            catch (Exception exception)
            {
                Log.WriteFormat(Level.Error, "An error occured when attempting to initialise the service: {0}", exception.Message);
                throw new FaultException($"Service Configuration Error: {exception.Message} ");
            }
        
        }

        public static T GetInstance<T>() where T : class
        {
            if(_container == null)
                throw new InvalidOperationException(ContainerNotSetMessage);
            
            return _container.GetInstance<T>();
        }

        public static object GetInstance(Type serviceType)
        {
            if (_container == null)
                throw new InvalidOperationException(ContainerNotSetMessage);

            return _container.GetInstance(serviceType);
        }
    }
}