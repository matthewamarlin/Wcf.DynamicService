﻿using System;
using System.ServiceModel;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using SpearOne.DynamicService.Contracts;

namespace SpearOne.DynamicService.Service
{
    // We use Dynamic here because we want to resolve the command / query types on the fly.
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Single, InstanceContextMode = InstanceContextMode.PerCall)]
    public class DynamicService : IDynamicService
    {
        private readonly ILogger _log = new Log4NetWrapper(typeof(DynamicService));

        public DynamicService()
        {
            _log.Write(Level.Debug, "Dynamic Service Started.");
        }

        public object ExecuteCommand(dynamic command)
        {
            _log.WriteFormat(Level.Debug, "ExecuteCommand called with [{0}]", command.GetType().Name);

            Type commandHandlerType = typeof(ICommandHandler<>).MakeGenericType(command.GetType());

            dynamic commandHandler = ServiceCompositionRoot.GetInstance(commandHandlerType);

            commandHandler.Handle(command);
            
            // In some situations, for example when performing DB operation  such as running a command that adds a DB item, 
            // an Id might generated on the server side. This is why we return the command so we can get access to the Id.
            
            // Instead of returning the output property of a command, we just return the complete command.
            // There is some overhead in this, but is of course much easier than returning a part of the command.
            return command;
        }


        public object ExecuteQuery(dynamic query)
        {
            _log.WriteFormat(Level.Debug, "ExecuteQuery called with [{0}]", query.GetType().Name);

            Type queryType = query.GetType();
            Type resultType = KnownTypeResolver.GetQueryResultType(queryType);
            Type queryHandlerType = typeof(IQueryHandler<,>).MakeGenericType(queryType, resultType);

            dynamic queryHandler = ServiceCompositionRoot.GetInstance(queryHandlerType);

            return queryHandler.Handle(query);
        }
    }
}
