﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.DynamicService.Service")]
[assembly: AssemblyDescription("The WCF service implementation for the SpearOne.DynamicService")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("5DFA138B-AF58-4148-B099-A98A5DFB1E7E")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]