﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.DynamicService.Hosts.WindowsService.NetTcp")]
[assembly: AssemblyDescription("An implementation of the SpearOne.DynamicService built as a WindowsService host using the NetTcp protocol.")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("44BA9A07-614B-4FE9-B255-81C7AB073082")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]
