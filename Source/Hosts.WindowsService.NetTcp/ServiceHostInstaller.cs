﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace SpearOne.DynamicService.Hosts.WindowsService.NetTcp
{
    [RunInstaller(true)]
    public partial class ServiceHostInstaller : Installer
    {
        public ServiceHostInstaller()
        {
            //InitializeComponent();
            Install();
        }

        public void Install()
        {
            Installers.Add(new ServiceInstaller
            {
                ServiceName = "DynamicService_NetTcp",
                DisplayName = "SpearOne - Dynamic Service [net.tcp]",
                Description = "Hosts the DynamicService using the Net.Tcp Protocol",
                StartType = ServiceStartMode.Automatic
            });

            Installers.Add(new ServiceProcessInstaller { Account = ServiceAccount.LocalSystem });
        }
    }
}
