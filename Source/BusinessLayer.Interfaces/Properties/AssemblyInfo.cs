﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.DynamicService.BusinessLayer.Interfaces")]
[assembly: AssemblyDescription("An assembly containing the BusinessLayer interfaces for the SpearOne.DynamicService.")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("69B0679F-1FA0-44B3-8CC0-6FBFA42E136D")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]