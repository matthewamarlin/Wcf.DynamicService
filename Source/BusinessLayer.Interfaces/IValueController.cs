﻿using SpearOne.DynamicService.DataTransferObjects;

namespace SpearOne.DynamicService.BusinessLayer.Interfaces
{
    public interface IValueController
    {
        void SetValue(Value value);
        Value GetValue();
    }
}
