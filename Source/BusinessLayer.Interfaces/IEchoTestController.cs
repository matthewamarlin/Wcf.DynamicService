﻿namespace SpearOne.DynamicService.BusinessLayer.Interfaces
{
    public interface IEchoTestController
    {
        string Echo(string message);
    }
}
