﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace SpearOne.DynamicService.Hosts.WindowsService.Https
{
    [RunInstaller(true)]
    public partial class ServiceHostInstaller : Installer
    {
        public ServiceHostInstaller()
        {
            //InitializeComponent();
            Install();
        }

        public void Install()
        {
            Installers.Add(new ServiceInstaller
            {
                ServiceName = "DynamicService_Https",
                DisplayName = "SpearOne - Dynamic Service [Https]",
                Description = "Hosts the DynamicService using the Https Protocol",
                StartType = ServiceStartMode.Automatic
            });

            Installers.Add(new ServiceProcessInstaller { Account = ServiceAccount.LocalSystem });
        }
    }
}
