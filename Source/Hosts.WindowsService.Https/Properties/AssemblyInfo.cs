﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.DynamicService.Hosts.WindowsService.Https")]
[assembly: AssemblyDescription("An implementation of the SpearOne.DynamicService built as a WindowsService host using the Https protocol.")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("CA9DC40F-555B-4098-8118-171E2BE09CB6")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]
