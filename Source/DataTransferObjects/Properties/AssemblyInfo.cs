﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.DynamicService.DataTransferObjects")]
[assembly: AssemblyDescription("An assembly containing the DataTransferObject for use when communicating with the SpearOne.DynamicService.")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("254824F2-CD4C-4F34-AC74-5F97459C86B6")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]