﻿namespace SpearOne.DynamicService.Contracts
{
    public interface ICommandHandler<in TCommand>
    {
        void Handle(TCommand command);
    }
}
