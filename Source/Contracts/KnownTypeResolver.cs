﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SpearOne.DynamicService.Contracts
{
    public static class KnownTypeResolver
    {
        public static IEnumerable<Type> GetKnownCommandTypes(ICustomAttributeProvider provider)
        {
            var coreAssembly = typeof(ICommandHandler<>).Assembly;

            // We use convention over configuration to find all of the Commands.
            var commandTypes =
                from type in coreAssembly.GetExportedTypes()
                where type.Name.EndsWith("Command")
                select type;

            return commandTypes.ToArray();
        }

        public static IEnumerable<Type> GetKnownQueryTypes(ICustomAttributeProvider provider)
        {
            var contractAssembly = typeof(IQuery<>).Assembly;

            var queryTypes = (
                from type in contractAssembly.GetExportedTypes()
                where TypeIsQueryType(type)
                select type)
                .ToList();

            var resultTypes =
                from queryType in queryTypes
                select GetQueryResultType(queryType);

            return queryTypes.Union(resultTypes).ToArray();
        }

        public static Type GetQueryResultType(Type queryType)
        {
            return GetQueryInterface(queryType).GetGenericArguments()[0];
        }

        private static bool TypeIsQueryType(Type type)
        {
            return GetQueryInterface(type) != null;
        }

        private static Type GetQueryInterface(Type type)
        {
            return (
                from @interface in type.GetInterfaces()
                where @interface.IsGenericType
                where typeof(IQuery<>).IsAssignableFrom(@interface.GetGenericTypeDefinition())
                select @interface)
                .SingleOrDefault();
        }
    }
}
