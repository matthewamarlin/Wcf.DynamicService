﻿using System.ServiceModel;

namespace SpearOne.DynamicService.Contracts
{
    [ServiceContract(Namespace = Constants.Namespace)]
    public interface IDynamicService
    {
        [OperationContract]
        [ServiceKnownType("GetKnownCommandTypes", typeof(KnownTypeResolver))]
        object ExecuteCommand(dynamic command);
        
        [OperationContract]
        [ServiceKnownType("GetKnownQueryTypes", typeof(KnownTypeResolver))]
        object ExecuteQuery(dynamic command);
    }
}
