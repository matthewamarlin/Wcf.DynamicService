﻿using System;

namespace SpearOne.DynamicService.Contracts.Queries
{
    public class EchoTestQuery : IQuery<string>
    {
        public EchoTestQuery()
        {
            Message = string.Empty;
        }

        public EchoTestQuery(string message )
        {
            Message = message;
        }

        public string Message { get; set; }

        public override string ToString()
        {
            return String.Format("EchoTestQuery - Message: '{0}'", Message);
        }
    }
}