﻿using SpearOne.DynamicService.DataTransferObjects;

namespace SpearOne.DynamicService.Contracts.Queries
{
    public class GetValueQuery : IQuery<Value>
    {
        //We dont need any data here due to the simplicity of the example
    }
}
