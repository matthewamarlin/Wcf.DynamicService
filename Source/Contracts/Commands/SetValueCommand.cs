﻿using System;
using SpearOne.DynamicService.DataTransferObjects;

namespace SpearOne.DynamicService.Contracts.Commands
{
    public class SetValueCommand 
    {
        //Must have a default constructor for DataContractSerialization
        public SetValueCommand()
        {
            Value = new Value();
        }

        public SetValueCommand(Value value)
        {
            Value = value;
        }

        public Value Value { get; set; }

        public override string ToString()
        {
            return String.Format("Value: {0}", Value);
        }
    }
}
