﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.DynamicService.Contracts")]
[assembly: AssemblyDescription("The contract used to communicate with the SpearOne.DynamicService")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("9FAF18D0-FE13-42F4-A901-D09D03184532")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]