<#
    Generates a service proxy using SvcUtil.exe for HTTPS services.
    SvcUtil.exe.config is used to configure the client certifice used when connecting to the service.
    The generated service reference class is then moved to the project directory.
#> 

$ServiceName = "DynamicService"
$ServieEndpointURL = "https://localhost:9210/$ServiceName"
$ClientCertificateThumbprint = "5ca1c3039106f5d093974a1c67f330c625d57b3f" #You need to add this certificate to the appropriate certificate store.

$ClientCertConfigSnippet = @"
<behaviors>
      <endpointBehaviors>
        <behavior name="ClientCertificateBehaviour">
          <clientCredentials>
            <clientCertificate findValue="$ClientCertificateThumbprint"
                  storeLocation="CurrentUser"
                  storeName="My"
                  x509FindType="FindByThumbprint" />
          </clientCredentials>
        </behavior>
      </endpointBehaviors>
</behaviors>
"@

try
{
    if([String]::IsNullOrWhiteSpace($PSScriptRoot)){
        $PSScriptRoot = Split-Path $MyInvocation.MyCommand.Path -Parent
    }
    
    cd $PSScriptRoot

    Write-Host "Generation started..." -ForegroundColor Green

    Write-Host "`Creating service proxy class using SvcUtil.exe" -ForegroundColor Magenta

    if(-not (Test-Path ".\SvcUtil.exe"))
    {
        Copy-Item 'C:\Program Files (x86)\Microsoft SDKs\Windows\v8.0A\bin\NETFX 4.0 Tools\SvcUtil.exe' -Destination ".\SvcUtil.exe"
    }

    .\SvcUtil.exe "$ServieEndpointURL" "/config:app"

    $Doc = New-Object System.Xml.XmlDocument
    $Doc.Load(".\app.config")

    $Fragment = $Doc.CreateDocumentFragment();
    $Fragment.InnerXml = $ClientCertConfigSnippet;
	
    $node = $Doc.SelectSingleNode("/configuration/system.serviceModel")

    Write-Host "`nConfiguring the generated app.config for client certificate." -ForegroundColor Magenta
        
    $node.AppendChild($Fragment) | Out-Null

    $node = $Doc.SelectSingleNode("/configuration/system.serviceModel/client/endpoint")

    $node.SetAttribute("behaviourConfiguration", "ClientCertificateBehaviour")

    $Doc.Save(".\app.config")

    Write-Host "`nMoving generated service class to project directory." -ForegroundColor Magenta
    
    Move-Item -Path ".\$ServiceName.cs" -Destination "..\$ServiceName.cs" -Force

    Write-Host "`nProxy Generation Completed" -ForegroundColor Green

    Remove-Item .\SvcUtil.exe
}
catch
{
    Write-Host "Proxy Generation Failed: $_" -ForegroundColor Red
}

