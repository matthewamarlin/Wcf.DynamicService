﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.DynamicService.Proxy")]
[assembly: AssemblyDescription("A client proxy implementation for theSpearOne.DynamicService. This proxy exposes each service operation " +
                               "as a method call instead of using the dynamic type engine.")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("CAAC8268-9CCB-4DF9-BA51-6D08E72AAF10")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]