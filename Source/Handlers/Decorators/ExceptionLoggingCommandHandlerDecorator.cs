﻿using System;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using SpearOne.DynamicService.Contracts;

namespace SpearOne.DynamicService.Handlers.Decorators
{
    public class ExceptionLoggingCommandHandlerDecorator<TCommand>: ICommandHandler<TCommand>
    {
        private readonly ICommandHandler<TCommand> _innerCommandHandler;
        
        private readonly ILogger _log = new Log4NetWrapper(typeof(ExceptionLoggingCommandHandlerDecorator<TCommand>));

        public ExceptionLoggingCommandHandlerDecorator(ICommandHandler<TCommand> innerCommandHandler)
        {
            if(innerCommandHandler == null)
                throw new ArgumentNullException(nameof(innerCommandHandler));

            _innerCommandHandler = innerCommandHandler;
        }

        public void Handle(TCommand command)
        {
            try
            {
                _innerCommandHandler.Handle(command);
            }
            catch (Exception exception)
            {
                _log.WriteFormat(Level.Error, "An error occured when executing [{0}], the following exception occured [{1}]", _innerCommandHandler.GetType().Name, exception.ToString());
                throw;
            }
        }
    }
}
