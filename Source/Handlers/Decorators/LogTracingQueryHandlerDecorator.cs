﻿using System;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using SpearOne.DynamicService.Contracts;

namespace SpearOne.DynamicService.Handlers.Decorators
{
  
    public class LogTracingQueryHandlerDecorator<TQuery, TResult> : IQueryHandler<TQuery, TResult> 
        where TQuery : IQuery<TResult>
    {
        private readonly IQueryHandler<TQuery, TResult> _innerQueryHandler;
        
        private readonly ILogger _log = new Log4NetWrapper(typeof(LogTracingQueryHandlerDecorator<TQuery, TResult>));

        public LogTracingQueryHandlerDecorator(IQueryHandler<TQuery, TResult> innerQueryHandler)
        {
            if(innerQueryHandler == null)
                throw new ArgumentNullException(nameof(innerQueryHandler));

            _innerQueryHandler = innerQueryHandler;
        }

        public TResult Handle(TQuery query)
        {
            var handlerType = _innerQueryHandler.GetType().Name;
            
            _log.WriteFormat(Level.Info, "Entering query handler [{0}] with query [{1}]", handlerType, query.ToString());
            
            var result = _innerQueryHandler.Handle(query);
            
            _log.WriteFormat(Level.Trace, "Exiting query handler [{0}]", handlerType);
            
            return result;
        }
    }
}
