﻿using System;
using System.Linq;
using SpearOne.Common.Extensions;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using SpearOne.DynamicService.Contracts;

namespace SpearOne.DynamicService.Handlers.Decorators
{
    public class ExceptionLoggingQueryHandlerDecorator<TQuery, TResult> : IQueryHandler<TQuery, TResult> 
        where TQuery : IQuery<TResult>
    {
        private readonly IQueryHandler<TQuery, TResult> _innerQueryHandler;
        
        private readonly ILogger _log = new Log4NetWrapper(typeof(ExceptionLoggingQueryHandlerDecorator<TQuery, TResult>));

        public ExceptionLoggingQueryHandlerDecorator(IQueryHandler<TQuery, TResult> innerQueryHandler)
        {
            if(innerQueryHandler == null)
                throw new ArgumentNullException(nameof(innerQueryHandler));

            _innerQueryHandler = innerQueryHandler;
        }

        public TResult Handle(TQuery query)
        {
            try
            {
                return _innerQueryHandler.Handle(query);
            }
            catch (Exception exception)
            {
                var messages = String.Join(Environment.NewLine, exception.FromHierarchy(x => x.InnerException).Select(x => x.Message));
                _log.WriteFormat(Level.Error, "An error occured when executing [{0}], the following exception occured [{1}]", _innerQueryHandler.GetType(), messages);
                throw;
            }
        }
    }
}
