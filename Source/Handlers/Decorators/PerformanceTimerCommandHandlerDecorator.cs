﻿using System;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using SpearOne.DynamicService.Contracts;

namespace SpearOne.DynamicService.Handlers.Decorators
{
    public class PerformanceTimerCommandHandlerDecorator<TCommand> : ICommandHandler<TCommand>
    {
        private readonly ICommandHandler<TCommand> _innerCommandHandler;

        private readonly ILogger _log = new Log4NetWrapper(typeof(PerformanceTimerCommandHandlerDecorator<TCommand>));

        public PerformanceTimerCommandHandlerDecorator(ICommandHandler<TCommand> innerCommandHandler)
        {
            if (innerCommandHandler == null)
                throw new ArgumentNullException(nameof(innerCommandHandler));

            _innerCommandHandler = innerCommandHandler;
        }

        public void Handle(TCommand command)
        {
            var start = DateTime.Now;

            _innerCommandHandler.Handle(command);

            var totalTime = (DateTime.Now - start).ToString();

            _log.WriteFormat(Level.Debug, "Command [{0}] Completed in [{1}]", typeof(TCommand).Name, totalTime);
        }
    }
}