﻿using System;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using SpearOne.DynamicService.Contracts;

namespace SpearOne.DynamicService.Handlers.Decorators
{
    public class PerformanceTimerQueryHandlerDecorator<TQuery, TResult> : IQueryHandler<TQuery, TResult> where TQuery: IQuery<TResult>
    {
        private readonly IQueryHandler<TQuery, TResult> _innerQueryHandler;

        private readonly ILogger _log = new Log4NetWrapper(typeof(PerformanceTimerQueryHandlerDecorator<TQuery, TResult>));

        public PerformanceTimerQueryHandlerDecorator(IQueryHandler<TQuery, TResult> innerQueryHandler)
        {
            if (innerQueryHandler == null)
                throw new ArgumentNullException(nameof(innerQueryHandler));

            _innerQueryHandler = innerQueryHandler;
        }

        public TResult Handle(TQuery query)
        {
            var start = DateTime.Now;

            var result = _innerQueryHandler.Handle(query);

            var totalTime = (DateTime.Now - start);

            _log.WriteFormat(Level.Info, "Query [{0}] completed in [{1:c}] with result [{2}]", typeof(TQuery).Name, totalTime, result);

            return result;
        }
    }
}