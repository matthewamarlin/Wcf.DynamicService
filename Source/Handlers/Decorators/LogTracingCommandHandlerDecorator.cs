﻿using System;
using SpearOne.Common.Logging;
using SpearOne.Common.Logging.Log4Net;
using SpearOne.DynamicService.Contracts;

namespace SpearOne.DynamicService.Handlers.Decorators
{
    public class LogTracingCommandHandlerDecorator<TCommand> : ICommandHandler<TCommand>
    {
        private readonly ICommandHandler<TCommand> _innerCommandHandler;

        private readonly ILogger _log = new Log4NetWrapper(typeof(LogTracingCommandHandlerDecorator<TCommand>));

        public LogTracingCommandHandlerDecorator(ICommandHandler<TCommand> innerCommandHandler)
        {
            if (innerCommandHandler == null)
                throw new ArgumentNullException(nameof(innerCommandHandler));

            _innerCommandHandler = innerCommandHandler;
        }

        public void Handle(TCommand command)
        {
            var handlerType = _innerCommandHandler.GetType().Name;

            _log.WriteFormat(Level.Info, "Entering command handler [{0}] with command [{1}]", handlerType, command.ToString());
            _innerCommandHandler.Handle(command);
            _log.WriteFormat(Level.Trace, "Exiting command handler: [{0}]", handlerType);
        }
    }
}
