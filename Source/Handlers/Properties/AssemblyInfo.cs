﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.DynamicService.Handlers")]
[assembly: AssemblyDescription("Command & Query Handler implementation for the SpearOne.DynamicService")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("61B6D7A9-BDCA-4FD8-B156-F7D9103A3147")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]