﻿using SpearOne.DynamicService.BusinessLayer.Interfaces;
using SpearOne.DynamicService.Contracts;
using SpearOne.DynamicService.Contracts.Queries;
using SpearOne.DynamicService.DataTransferObjects;

namespace SpearOne.DynamicService.Handlers.QueryHandlers
{
    public class GetValueQueryHandler : IQueryHandler<GetValueQuery, Value>
    {
        private readonly IValueController _valueController;

        public GetValueQueryHandler(IValueController valueController)
        {
            _valueController = valueController;
        }

        public Value Handle(GetValueQuery query)
        {
            return _valueController.GetValue();
        }
    }
}
