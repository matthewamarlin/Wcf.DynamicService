﻿using SpearOne.DynamicService.BusinessLayer.Interfaces;
using SpearOne.DynamicService.Contracts;
using SpearOne.DynamicService.Contracts.Queries;

namespace SpearOne.DynamicService.Handlers.QueryHandlers
{
    public class EchoTestQueryHandler : IQueryHandler<EchoTestQuery, string>
    {
        private readonly IEchoTestController _echoTestController;

        public EchoTestQueryHandler(IEchoTestController echoTestController)
        {
            _echoTestController = echoTestController;
        }

        public string Handle(EchoTestQuery query)
        {
            return _echoTestController.Echo(query.Message);
        }
    }
}
