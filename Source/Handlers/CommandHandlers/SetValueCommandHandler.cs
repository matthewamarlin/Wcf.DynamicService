﻿using SpearOne.DynamicService.BusinessLayer.Interfaces;
using SpearOne.DynamicService.Contracts;
using SpearOne.DynamicService.Contracts.Commands;

namespace SpearOne.DynamicService.Handlers.CommandHandlers
{
    public class SetValueCommandHandler : ICommandHandler<SetValueCommand>
    {
        private readonly IValueController _valueController;

        public SetValueCommandHandler(IValueController valueController)
        {
            _valueController = valueController;
        }

        public void Handle(SetValueCommand command)
        {
            _valueController.SetValue(command.Value);
        }
    }
}
