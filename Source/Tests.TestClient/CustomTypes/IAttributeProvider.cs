﻿using System;
using System.Reflection;

namespace SpearOne.DynamicService.Tests.TestClient.CustomTypes
{
    public interface IAttributeProvider
    {
        Attribute[] ProvideAttributesFor(PropertyInfo property);

        Attribute[] ProvideAttributesFor(Object @object);
    }
}
