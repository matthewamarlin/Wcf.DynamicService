﻿using System;
using System.ComponentModel;

namespace SpearOne.DynamicService.Tests.TestClient.CustomTypes
{
    public interface IAttributeInjectionObjectWrapper : ICustomTypeDescriptor, INotifyPropertyChanged
    {
        /// <summary>
        /// The original Member which was wrapped as an object.
        /// </summary>
        Object OriginalObject { get; }
    }
}
