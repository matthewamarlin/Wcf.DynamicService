﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using SpearOne.Common.Reflection;

namespace SpearOne.DynamicService.Tests.TestClient.CustomTypes
{
    public class AttributeInjectionObjectWrapper : IAttributeInjectionObjectWrapper
    {
        #region Storage Mechanism

        /// <summary>
        /// This Dictionary is used to store all the properties within the object with their associated object.
        /// The Implementation of the ICustomTypeDecriptor uses these 'Wrapped Properties' when reflecting for 
        /// type information.
        /// </summary>
        private readonly IDictionary<string, object> _wrappedProperties = new Dictionary<string, object>();

        /// <summary>
        /// This is used to store the original member that has been wrapped.
        /// The properties of which are kept up to date with the _wrappedProperties.
        /// </summary>
        private readonly Object _originalObject;

        public object OriginalObject
        {
            get { return _originalObject; }
        }

        #endregion

        #region Fuctional Members

        private readonly IAttributeProvider _attributeProvider;

        #endregion

        #region Constructors

        public AttributeInjectionObjectWrapper(Object originalObject,
            IAttributeProvider attributeProvider, bool recurse = true)
        {
            _originalObject = originalObject;
            _attributeProvider = attributeProvider;

            if (originalObject.GetType().IsSystemType() && recurse)
            {
                throw new Exception("Cant recurse into a system type, try turning specifying the recurse flag as false.");
            }

            foreach (var property in OriginalObject.GetType().GetProperties())
            {
                if (recurse)
                {
                    if (property.PropertyType.IsSystemType())
                    {
                        _wrappedProperties[property.Name] = OriginalObject.GetPropertyValue(property.Name);
                        /*
                         * Two issues with wrapping system types:
                         * 
                         * - The line below can work (after changing a few things), but it has one caveat, the system types can no longer be edited because 
                         * they have been wrapped by a type the property grid doesnt have an editor for.
                         * In order to fix this we will have to implement our own custom type editor which servers as a wrapper for the default editors. 
                         * (This is no easy task)
                         * 
                         * - Our Reflection Helpers GetProperty fails for some of the Properties in the Type.
                         *      For example, a String type is problematic.
                         */
                        //_wrappedProperties[property.Name] = Wrap(originalObject, property, false);
                    }
                    else
                    {
                        _wrappedProperties[property.Name] = Wrap(originalObject, property, true);
                    }
                }
                else
                {
                    var result = OriginalObject.GetPropertyValue(property.Name);

                    _wrappedProperties[property.Name] = result;
                }
            }

            //Iterate over all the properties and recursively apply the appropriate attribute injection wrapper.
        }

        /// <summary>
        /// Wraps the specified property of the source object and optionally recurses the properties with the recurse Flag
        /// </summary>
        /// <param name="source"></param>
        /// <param name="property"></param>
        /// <param name="recurse"></param>
        /// <returns></returns>
        private dynamic Wrap(Object source, PropertyInfo property, bool recurse)
        {
            return new AttributeInjectionObjectWrapper(source.GetPropertyValue(property.Name), _attributeProvider, recurse);

            //return ReflectionHelper.CreateGenericTypeWithConstructor(
            //    typeof(AttributeInjectionObjectWrapper<>), property.PropertyType, //Type Parameters for generic Type
            //    new[] { property.PropertyType, typeof(IAttributeProvider), typeof(bool) }, //Constructor Signature
            //    new[] { ReflectionHelper.GetPropertyValue(source, property.Name), _attributeProvider, recurse }); //Constructor Arguments
        }

        #endregion

        #region Methods

        public override string ToString()
        {
            return OriginalObject.ToString();
        }

        private DynamicPropertyDescriptor[] GetWrappedPropertyDescriptors()
        {
            var dynamicPropertyDescriptors = new List<DynamicPropertyDescriptor>();

            foreach (var wrappedProperty in _wrappedProperties)
            {
                var propertyAttributes = new List<Attribute>();
                var originalObjectPropertyInfo = OriginalObject.GetType().GetProperty(wrappedProperty.Key);

                propertyAttributes.AddRange
                (
                    from object originalAttribute
                    in TypeDescriptor.GetAttributes(wrappedProperty.Value)
                    select originalAttribute as Attribute
                );

                propertyAttributes.AddRange(_attributeProvider.ProvideAttributesFor(originalObjectPropertyInfo));

                dynamicPropertyDescriptors.Add(new DynamicPropertyDescriptor(this, wrappedProperty.Key,  originalObjectPropertyInfo.PropertyType , propertyAttributes.ToArray()));
            }

            return dynamicPropertyDescriptors.ToArray();
        }

        private void SetOriginalObjectProperty(string propertyName, object value)
        {
            var originalObjectProperty = OriginalObject.GetType()
                                                       .GetProperties()
                                                       .FirstOrDefault(property => property.Name == propertyName);

            if (originalObjectProperty == null)
                throw new Exception(String.Format("An error occured when trying to find a Property within the OriginalObject of type '{0}' " +
                                                  "A Property with the name '{1}' could not be found.", OriginalObject.GetType(), propertyName));
            
            originalObjectProperty.SetValue(OriginalObject, value, null);

            OnPropertyChanged(propertyName);
        }

        #endregion

        #region Implementation of ICustomTypeDescriptor

        public AttributeCollection GetAttributes()
        {
            var originalAttributes = TypeDescriptor.GetAttributes(OriginalObject);

            var newAttributes = _attributeProvider.ProvideAttributesFor(OriginalObject);

            return AttributeCollection.FromExisting(originalAttributes, newAttributes);
        }

        public PropertyDescriptorCollection GetProperties()
        {
            if (_wrappedProperties == null)
            {
                return TypeDescriptor.GetProperties(OriginalObject);
            }

            return new PropertyDescriptorCollection(GetWrappedPropertyDescriptors());
        }

        public PropertyDescriptorCollection GetProperties(Attribute[] attributes)
        {
            if (_wrappedProperties == null)
            {
                return TypeDescriptor.GetProperties(OriginalObject);
            }

            var propertyDescriptorArray = GetWrappedPropertyDescriptors()
                    .Where(dynamicPropertyDescriptor => dynamicPropertyDescriptor.Attributes.Contains(attributes))
                    .ToArray();

            return new PropertyDescriptorCollection(propertyDescriptorArray);
        }

        public string GetClassName()
        {
            return TypeDescriptor.GetClassName(OriginalObject);
        }

        public EventDescriptorCollection GetEvents(Attribute[] attributes)
        {
            return TypeDescriptor.GetEvents(OriginalObject, attributes);
        }

        public EventDescriptorCollection GetEvents()
        {
            return TypeDescriptor.GetEvents(OriginalObject);
        }

        public EventDescriptor GetDefaultEvent()
        {
            return TypeDescriptor.GetDefaultEvent(OriginalObject);
        }

        public object GetPropertyOwner(PropertyDescriptor pd)
        {
            // In order to implement this we need to keep track of the objects 
            // owner by passing the name/reference in the constructor
            throw new NotImplementedException("GetPropertyOwner");
        }

        public string GetComponentName()
        {
            return TypeDescriptor.GetComponentName(OriginalObject);
        }

        public TypeConverter GetConverter()
        {
            return TypeDescriptor.GetConverter(OriginalObject);
        }

        public PropertyDescriptor GetDefaultProperty()
        {
            return TypeDescriptor.GetDefaultProperty(OriginalObject);
        }

        public object GetEditor(Type editorBaseType)
        {
            //return new PropertyGridEditorDateTimeUpDown();

            return TypeDescriptor.GetEditor(OriginalObject, editorBaseType);
        }

        #endregion

        #region Implementation of INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged == null)
            {
                return;
            }

            var eventArgs = new PropertyChangedEventArgs(propertyName);
            PropertyChanged(this, eventArgs);
        }

        #endregion

        #region DynamicPropertyDescriptor Subclass

        private class DynamicPropertyDescriptor : PropertyDescriptor
        {
            private readonly AttributeInjectionObjectWrapper _attributeInjectionObjectWrapper;
            private readonly Type _propertyType;

            public DynamicPropertyDescriptor(AttributeInjectionObjectWrapper attributeInjectionObjectWrapper,
                string propertyName, Type propertyType, Attribute[] propertyAttributes)
                : base(propertyName, propertyAttributes)
            {
                _attributeInjectionObjectWrapper = attributeInjectionObjectWrapper;
                _propertyType = propertyType;
            }

            public override bool CanResetValue(object component)
            {
                return true;
            }

            public override object GetValue(object component)
            {
                return _attributeInjectionObjectWrapper._wrappedProperties[Name];
            }

            public override void ResetValue(object component)
            {
                ReflectionHelpers.GetDefault(_propertyType);
            }

            public override void SetValue(object component, object value)
            {
                _attributeInjectionObjectWrapper.SetOriginalObjectProperty(Name, value);
                _attributeInjectionObjectWrapper._wrappedProperties[Name] = value;
                _attributeInjectionObjectWrapper.OnPropertyChanged(Name); // Not working it seems
            }

            public override bool ShouldSerializeValue(object component)
            {
                return false;
            }

            public override Type ComponentType
            {
                get { return typeof(object); }
            }

            public override bool IsReadOnly
            {
                get { return false; }
            }

            public override Type PropertyType
            {
                get { return _propertyType; }
            }
        }

        #endregion
    }
}