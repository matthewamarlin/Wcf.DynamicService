﻿using System;
using System.Collections.Generic;
using System.Reflection;
using SpearOne.Common.Reflection;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;

namespace SpearOne.DynamicService.Tests.TestClient.CustomTypes
{
    public class ExpandableObjectAttributeInjector : IAttributeProvider
    {
        public Attribute[] ProvideAttributesFor(PropertyInfo property)
        {
            var attributes = new List<Attribute>();

            if (!property.PropertyType.IsSystemType())
            {
                attributes.Add(new ExpandableObjectAttribute());
            }

            return attributes.ToArray();
        }

        // This will only work over non system types, since we dont wrap them.
        public Attribute[] ProvideAttributesFor(object @object)
        {
            return new Attribute[0]; //Do Nothing
        }
    }
}
