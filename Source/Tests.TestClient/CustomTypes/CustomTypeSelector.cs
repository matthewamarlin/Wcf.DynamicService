﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SpearOne.DynamicService.Tests.TestClient.CustomTypes
{
    public static class CustomTypeSelector
    {
        public static List<Type> GetCustomTypes()
        {
            return Assembly.GetExecutingAssembly().GetTypes().ToList();
        }
    }
}
