﻿using System.ComponentModel;

namespace SpearOne.DynamicService.Tests.TestClient.PresentationPattern
{
    class ObservableObject : INotifyPropertyChanged
    {

        protected virtual void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
