﻿using System;
using System.Windows;
using SpearOne.DynamicService.Tests.TestClient.CustomTypes;
using SpearOne.DynamicService.Tests.TestClient.Models;
using SpearOne.DynamicService.Tests.TestClient.ViewModels;

namespace SpearOne.DynamicService.Tests.TestClient.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly MainWindowViewModel _viewModel;
        readonly DynamicServiceClientModel _model = new DynamicServiceClientModel("SpearOne.DynamicService.Contracts");

        public MainWindow()
        {
            InitializeComponent();
            _viewModel = new MainWindowViewModel(_model);
            DataContext = _viewModel;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var wrappedObject = _viewModel.SelectedObject as IAttributeInjectionObjectWrapper;
            var endpoint = _viewModel.SelectedEndpoint;


            if (wrappedObject == null)
            {
                _viewModel.Output = "Please select an action to execute.";
                return;
            }

            if (endpoint == null)
            {
                _viewModel.Output = "Please select an endpoint address.";
                return;
            }

           _viewModel.Output = _model.Execute(wrappedObject.OriginalObject, endpoint);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            _viewModel.Output = String.Empty;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //var def = new EditorTemplateDefinition();
            //def.TargetProperties.Add(typeof(object));
            //def.EditingTemplate = (DataTemplate)FindResource("CustomTypeEditorTemplate");

            //PropertyGrid.EditorDefinitions.Add(def);
        }
    }
}
