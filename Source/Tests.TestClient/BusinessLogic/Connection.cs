﻿using System;
using System.ServiceModel;
using SpearOne.DynamicService.Contracts;

namespace SpearOne.DynamicService.Tests.TestClient.BusinessLogic
{
    public class Connection : IDisposable
    {
        private readonly IDynamicService _dynamicService;

        public Connection(string endpointName, string endpointAddress)
        {
            var channelFactory = new ChannelFactory<IDynamicService>(endpointName, new EndpointAddress(endpointAddress));
            _dynamicService = channelFactory.CreateChannel();
        }

        public dynamic ExecuteQuery(dynamic query)
        {
            return _dynamicService.ExecuteQuery(query);
        }

        public void ExecuteCommand(dynamic command)
        {
            _dynamicService.ExecuteCommand(command);
        }

        public void Dispose()
        {
            var connection = (_dynamicService as ICommunicationObject);

            if (connection != null)
            {
                try
                {
                    connection.Close();
                }
                catch (Exception)
                {
                    connection.Abort();
                }
            }
        }
    }
}
