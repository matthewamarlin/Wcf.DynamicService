﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.ServiceModel.Configuration;
using SpearOne.Common.Extensions;
using SpearOne.DynamicService.Tests.TestClient.Models;
using SpearOne.DynamicService.Tests.TestClient.PresentationPattern;

namespace SpearOne.DynamicService.Tests.TestClient.ViewModels
{
    internal class MainWindowViewModel : ObservableObject
    {
        private readonly DynamicServiceClientModel _model;

        
        public MainWindowViewModel(DynamicServiceClientModel model)
        {
            ActionList = new ObservableCollection<dynamic>();
            _model = model;
            
            Initialize();
        }

        private void Initialize()
        {
            try
            {
                EndpointList = new ObservableCollection<ChannelEndpointElement>(_model.GetEndpoints());
                ActionList = new ObservableCollection<dynamic>(_model.GetContracts());
            }
            catch (ReflectionTypeLoadException reflectionTypeLoadException)
            {
                var loaderExceptions = reflectionTypeLoadException.LoaderExceptions;

                Output = String.Format("An error occured when attempting to load the ContractsAssembly [{0}]\n", ContractsAssembly);

                foreach (var exceptionMessages in loaderExceptions.Select(exception => String.Join(Environment.NewLine, exception.FromHierarchy(x => x.InnerException).Select(ex => ex.Message))))
                {
                    Output += String.Format("[{0}]\n", exceptionMessages);
                }
            }
            catch (Exception exception)
            {

                var exceptionMessages = String.Join(Environment.NewLine,
                    exception.FromHierarchy(x => x.InnerException).Select(ex => ex.Message));

                Output = String.Format(
                    "An error occured when attempting to load the ContractsAssembly [{0}] \n\n[{1}]", ContractsAssembly,
                    exceptionMessages);
            }
        }

        public const string ContractsAssembly = "SpearOne.DynamicService.Contracts";

        #region Selected Action (Combobox)

        public ObservableCollection<dynamic> ActionList { get; set; }
        private dynamic _selectedAction;
        
        public dynamic SelectedAction
        {
            get { return _selectedAction; }
            set
            {
                if (_selectedAction != value)
                {
                    _selectedAction = value;

                    SelectedObject = _model.CreateTypeForDisplay(value);

                    RaisePropertyChanged("SelectedAction");
                }
            }
        }

        #endregion

        #region Selected Object (Properties Box)

        private dynamic _selectedObject;
        public dynamic SelectedObject
        {
            get { return _selectedObject; }
            set
            {
                if (_selectedObject != value)
                {
                    _selectedObject = value;

                    RaisePropertyChanged("SelectedObject");
                }
            }
        }

        #endregion

        #region Endpoint List

        public ObservableCollection<ChannelEndpointElement> EndpointList { get; set; }
        
        private ChannelEndpointElement _selectedEndpoint;
        public ChannelEndpointElement SelectedEndpoint
        {
            get { return _selectedEndpoint; }
            set
            {
                if (_selectedEndpoint != value)
                {
                    _selectedEndpoint = value;

                    RaisePropertyChanged("SelectedEndpoint");
                }
            }
        }

        #endregion

        #region Output

        private string _output;
        public string Output
        {
            get { return _output; }
            set
            {
                if (_output != value)
                {
                    _output = value;

                    RaisePropertyChanged("Output");
                }
            }
        }

        #endregion 
    }
}

