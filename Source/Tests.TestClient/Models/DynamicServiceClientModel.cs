﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.ServiceModel.Configuration;
using SpearOne.Common.Extensions;
using SpearOne.Common.Serialization;
using SpearOne.DynamicService.Tests.TestClient.BusinessLogic;
using SpearOne.DynamicService.Tests.TestClient.CustomTypes;

namespace SpearOne.DynamicService.Tests.TestClient.Models
{
    public class DynamicServiceClientModel
    {
        private readonly string _contractsAssembly;
        private IEnumerable<Type> _contractTypes; 

        internal DynamicServiceClientModel(string contractsAssembly)
        {
            _contractsAssembly = contractsAssembly;
        }

        public IEnumerable<dynamic> GetContracts()
        {
            _contractTypes = Assembly.Load(_contractsAssembly).GetTypes()
                    .Where(x => x.Namespace != null &&
                                (
                                    x.Namespace.Contains("Commands")
                                    || x.Namespace.Contains("Queries")
                                )
                    )
                    .ToList();

            foreach (var type in _contractTypes)
            {
                yield return type.Name;
            }
        }


        public IEnumerable<ChannelEndpointElement> GetEndpoints()
        {
            var clientSection = (ClientSection) ConfigurationManager.GetSection("system.serviceModel/client");

            foreach (ChannelEndpointElement endpoint in clientSection.Endpoints)
            {
                if (endpoint.Contract != "IMetadataExchange")
                {
                    yield return endpoint;   
                }
            }
        }

        public string Execute(dynamic action, ChannelEndpointElement endpoint)
        {
            try
            {
                using (var connection = new Connection(endpoint.Name, endpoint.Address.ToString()))
                {
                    if (action.GetType().Name.EndsWith("Query"))
                    {
                        var result = connection.ExecuteQuery(action);

                        return (result == null)
                            ? "The service returned the null value."
                            : DataContractSerialization.Serialize(result);
                    }
                    else if(action.GetType().Name.EndsWith("Command"))
                    {
                        connection.ExecuteCommand(action);
                       return "Command Complete";
                    }
                    else
                    {
                        return "Error command not run. Not a Command/Query.";
                    }

                }

            }
            catch (Exception exception)
            {
                return String.Join(Environment.NewLine + " -> ", exception.FromHierarchy(x => x.InnerException).Select(x => x.Message));
            }
        }

        public object CreateTypeForDisplay(string typeName)
        {
            var contractType = _contractTypes.FirstOrDefault(type => type.Name == typeName);

            if (contractType == null)
                throw new InvalidOperationException(String.Format("The application failed to find the corresponding contract Type for [{0}]", typeName));
            
            var instance = Activator.CreateInstance(contractType);

            return new AttributeInjectionObjectWrapper(instance, new ExpandableObjectAttributeInjector()); 
        }
    }
}
