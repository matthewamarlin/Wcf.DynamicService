﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace SpearOne.DynamicService.Tests.TestClient.UserControls
{
    /// <summary>
    /// Interaction logic for CustomTypeEditor.xaml
    /// </summary>
    public partial class CustomTypeEditor : UserControl, ITypeEditor
    {
        public CustomTypeEditor()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(
            "Value", typeof(IList<string>), typeof(CustomTypeEditor), new PropertyMetadata(default(IList<string>)));

        public IList<string> Value
        {
            get { return (IList<string>)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public FrameworkElement ResolveEditor(Xceed.Wpf.Toolkit.PropertyGrid.PropertyItem propertyItem)
        {
            var binding = new Binding("Value")
            {
                Source = propertyItem,
                Mode = propertyItem.IsReadOnly ? BindingMode.OneWay : BindingMode.TwoWay
            };
            BindingOperations.SetBinding(this, ValueProperty, binding);
            return this;
        }

        private void Button_OnClick(object sender, RoutedEventArgs e)
        {
            CustomTypeViewer viewer = new CustomTypeViewer { DataContext = this };
            viewer.ShowDialog();
        }
    }
}
