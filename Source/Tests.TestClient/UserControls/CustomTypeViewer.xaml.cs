﻿using System.Windows;

namespace SpearOne.DynamicService.Tests.TestClient.UserControls
{
    /// <summary>
    /// Interaction logic for CustomTypeViewer.xaml
    /// </summary>
    public partial class CustomTypeViewer : Window
    {
        public CustomTypeViewer()
        {
            InitializeComponent();
        }
    }
}
