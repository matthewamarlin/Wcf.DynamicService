﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("SpearOne.DynamicService.Hosts.WindowsService.Http")]
[assembly: AssemblyDescription("An implementation of the SpearOne.DynamicService built as a WindowsService host using the Http protocol.")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("2F315D7B-B5A2-4FF5-8396-8E323E4B7C62")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]