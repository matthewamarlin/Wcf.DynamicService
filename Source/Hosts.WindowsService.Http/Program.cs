﻿using SpearOne.DynamicService.WindowsService;

namespace SpearOne.DynamicService.Hosts.WindowsService.Http
{
    public class Program
    {
        public static void Main(string[] args)
        {
#if DEBUG
            new ServiceInitializer().StartConsoleHost(ServiceType.Dynamic);
#else
            new ServiceInitializer().StartServiceHost();
#endif
        }
    }
}
