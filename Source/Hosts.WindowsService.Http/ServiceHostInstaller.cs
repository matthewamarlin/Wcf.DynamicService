﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace SpearOne.DynamicService.Hosts.WindowsService.Http
{
    [RunInstaller(true)]
    public partial class ServiceHostInstaller : Installer
    {
        public ServiceHostInstaller()
        {
            //InitializeComponent();
            Install();
        }

        public void Install()
        {
            Installers.Add(new ServiceInstaller
            {
                ServiceName = "DynamicService_Http",
                DisplayName = "SpearOne - Dynamic Service [Http]",
                Description = "Hosts the DynamicService using the Http Protocol",
                StartType = ServiceStartMode.Automatic
            });

            Installers.Add(new ServiceProcessInstaller { Account = ServiceAccount.LocalSystem });
        }
    }
}
